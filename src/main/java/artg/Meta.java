package artg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "result_count", "cluster_counts", "clusters", "group_details" })
public class Meta implements Serializable {

	@JsonProperty("result_count")
	private Integer resultCount;
	@JsonProperty("cluster_counts")
	private ClusterCounts clusterCounts;
	@JsonProperty("clusters")
	private List<Object> clusters = null;
	@JsonProperty("group_details")
	private List<Object> groupDetails = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -5762051304192515298L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Meta() {
	}

	/**
	 * 
	 * @param clusterCounts
	 * @param clusters
	 * @param resultCount
	 * @param groupDetails
	 */
	public Meta(Integer resultCount, ClusterCounts clusterCounts, List<Object> clusters, List<Object> groupDetails) {
		super();
		this.resultCount = resultCount;
		this.clusterCounts = clusterCounts;
		this.clusters = clusters;
		this.groupDetails = groupDetails;
	}

	@JsonProperty("result_count")
	public Integer getResultCount() {
		return resultCount;
	}

	@JsonProperty("result_count")
	public void setResultCount(Integer resultCount) {
		this.resultCount = resultCount;
	}

	@JsonProperty("cluster_counts")
	public ClusterCounts getClusterCounts() {
		return clusterCounts;
	}

	@JsonProperty("cluster_counts")
	public void setClusterCounts(ClusterCounts clusterCounts) {
		this.clusterCounts = clusterCounts;
	}

	@JsonProperty("clusters")
	public List<Object> getClusters() {
		return clusters;
	}

	@JsonProperty("clusters")
	public void setClusters(List<Object> clusters) {
		this.clusters = clusters;
	}

	@JsonProperty("group_details")
	public List<Object> getGroupDetails() {
		return groupDetails;
	}

	@JsonProperty("group_details")
	public void setGroupDetails(List<Object> groupDetails) {
		this.groupDetails = groupDetails;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	
	@Override
    public String toString() {
        return "Meta{" +
                "result count='" + resultCount + 
                '}';
    }

}