package artg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "meta", "records" })
public class Example implements Serializable {

	@JsonProperty("meta")
	private Meta meta;
	@JsonProperty("records")
	private List<Record> records = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 6614498315170237697L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Example() {
	}

	/**
	 * 
	 * @param records
	 * @param meta
	 */
	public Example(Meta meta, List<Record> records) {
		super();
		this.meta = meta;
		this.records = records;
	}

	@JsonProperty("meta")
	public Meta getMeta() {
		return meta;
	}

	@JsonProperty("meta")
	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	@JsonProperty("records")
	public List<Record> getRecords() {
		return records;
	}

	@JsonProperty("records")
	public void setRecords(List<Record> records) {
		this.records = records;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	
	@Override
    public String toString() {
        return "Example{" + meta + " }";
    }

}