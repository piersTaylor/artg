package artg;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

/**
 * @author piers
 *
 */
@SpringBootApplication
public class ArtgApplication {

	private static final Scanner scan = new Scanner(System.in);

	private static final Random rand = new Random();

	public static void main(String args[]) {
		SpringApplication.run(ArtgApplication.class);
		LaunchGame();

	}

	public static void LaunchGame() {
		ShowTitleScreen(); // show title screen and ask if you want to play
		Example example = LoadData();
		ArrayList<QuestionData> questionData = LoadQuestions(example);
		ArrayList<String> dates = LoadDates(example);
		Boolean continuePlaying = true;
		while (continuePlaying) {
			Integer score = GameLoop(questionData, dates);
			System.out.println("you scored: " + score + "/10");
			continuePlaying = ContinuePlayingPrompt();
		}
		System.out.println("Ctrl-C to exit");

	}

	private static Boolean ContinuePlayingPrompt() {

		System.out.println("Another go? (y/n)");
		Character response = scan.next().charAt(0);
		if (response.equals('y')) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param questionData
	 * @param dates
	 * @return score
	 */
	private static Integer GameLoop(ArrayList<QuestionData> questionData, ArrayList<String> dates) {
		Integer score = 0;
		Integer counter = 1;
		ArrayList<String> answers = new ArrayList<String>();
		for (QuestionData question : questionData) {
			String correctAnswer = question.getDateText();
			answers.clear();
			while (answers.size() < 4) {
				String wrongAnswer = dates.get(rand.nextInt(dates.size() - 1));
				if (!(wrongAnswer.equals(correctAnswer))) {
					answers.add(wrongAnswer);
				}
			}
			System.out.println("-----");
			System.out.println(" -¦ Item " + counter);
			System.out.println("-----");
			if ("".equals(question.getObject())) {
				System.out.println("\"I'm not quite sure what this item is...\"");
			} else {
				System.out.println("\"Take a look at this " + question.getObject() + "!\"");
			}
			if ("".equals(question.getPlace())) {
				System.out.println("\"Its origins are unknown...\"");
			} else {
				System.out.println("\"It comes all the way from " + question.getPlace() + "!\"");
			}
			if ("".equals(question.getArtist())) {
				System.out.println("\"No one knows who made it...\"");
			} else {
				System.out.println("\"It was created by " + question.getArtist() + "!\"");
			}
			if ("".equals(question.getTitle())) {
				System.out.println("\"I have no idea what it's called...\"");
			} else {
				System.out.println("\"It's known as '" + question.getTitle() + "'!\"");
			}
			System.out.println();
			System.out.println("When do you think it was made?");
			answers.set(rand.nextInt(answers.size()), correctAnswer);
			int answerNumber = 0;
			for (String answer : answers) {
				System.out.println(answerNumber + ". " + answer);
				answerNumber++;
			}

			Integer givenAnswerNumber = -1;

			while (givenAnswerNumber < 0) {
				try {
					givenAnswerNumber = scan.nextInt();
				} catch (InputMismatchException e) {
					System.out.println("That answer was not valid. Please try again.");
					scan.nextLine();
				}
			}

			if ((givenAnswerNumber >= 0) && (givenAnswerNumber <= 3)) {
				if (correctAnswer.equals(answers.get(givenAnswerNumber))) {
					System.out.println("\"Correct!\"");
					score++;
				} else {
					System.out.println("\"That's incorrect, I'm afraid...\"");
				}

			}
			System.out.println();

			counter++;
		}
		return score;
	}

	/**
	 * @param example
	 * @return dates
	 */
	private static ArrayList<String> LoadDates(Example example) {
		ArrayList<String> dates = new ArrayList<>();
		String temp = "";
		for (Record record : example.getRecords()) {
			temp = record.getFields().getDateText();
			if (!("".equals(temp)) && (temp != null)) {
				dates.add(temp);
			}
		}
		return dates;
	}

	private static Example LoadData() {
		RestTemplate restTemplate = new RestTemplate();
		String URL = "https://www.vam.ac.uk/api/json/museumobject/search?random=1&offset=" + rand.nextInt(10000);
		return restTemplate.getForObject(URL, Example.class);
	}

	/**
	 * @param example
	 * @return questionList
	 */
	private static ArrayList<QuestionData> LoadQuestions(Example example) {
		Integer questionCount = 0;
		Integer index = 0;
		ArrayList<QuestionData> questionList = new ArrayList<QuestionData>();
		while (questionCount < 10) {
			Integer blanks = 0;
			Record record = example.getRecords().get(index);
			if (!("".equals(record.getFields().getDateText()))) {

				if ("".equals(record.getFields().getObject())) {
					blanks++;
				}
				if (("".equals(record.getFields().getArtist())) || ("Unknown".equals(record.getFields().getArtist()))) {
					blanks++;
				}
				if ("".equals(record.getFields().getPlace())) {
					blanks++;
				}
				if ("".equals(record.getFields().getTitle())) {
					blanks++;
				}

				if (blanks <= 1) {
					questionList.add(new QuestionData(record.getFields().getTitle(), record.getFields().getPlace(),
							record.getFields().getArtist(), record.getFields().getDateText(),
							record.getFields().getObject()));
					questionCount++;
				}
			}
			index++;
		}
		return questionList;
	}

	private static void ShowTitleScreen() {
		System.out.print(
"                                                                  \n"+
"                                                                 \n"+
"                                                                 \n"+
"                        `:/:://++++//::/:`                       \n"+
"                    `:/+syhmmdhhyyyyhhhyys+/:`                   \n"+
"                  //osmNy+:`            ./oyso//                 \n"+
"               `/+ommo-                     `/yo++`              \n"+
"              :ooNd:           `.------.       -soo:             \n"+
"             o+dm:          :smMMN/---:ohds.     -y/o            \n"+
"           `s/Ny`        `+mNs:hMs       -NMo     `s/s`          \n"+
"           s:No         +NNo`  hMs        /MN       y/o          \n"+
"          //my        .dMy`    hMs        +My       `d//         \n"+
"          s+N`       oMm:      hMs       /No`        o+y         \n"+
"         `+hs      .dMm:---....hMy.---:so/`          -ho.        \n"+
"         ./d/     :NNyso++//:::dMh:::/sNm+`          .d/:        \n"+
"         .+d/    sMd.          hMs     `sNN+`        :h+-        \n"+
"          soo  .dMs            hMs       `sNN+`      ssy         \n"+
"          o:m +Nm:             hMs         `oNNo`   .N:o         \n"+
"          `s/dMs`              hMs           `+dNs-`m+s`         \n"+
"          .sMy/                hMs              -sNdy/-          \n"+
" .--..-:+hdo.+oo            `-/dmd/-`            /h/sdmy+:-..--` \n"+
"   .:/+/:`   `o+s:                             -hd+o`  .:/+/:.   \n"+
"               :+os:                        `/dd++:              \n"+
"                 :/oso/`                 ./ymyo/:                \n"+
"                   .//ossso/:-.```.-:+shdhso//.                  \n"+
"                       -///osyyyyyyyyso///-                      \n"+
"                            `.---:--.`                           \n\n");
			System.out.println("--- Welcome to Antiques Roadshow : The Game ---");
			System.out.println();
			
	}

}