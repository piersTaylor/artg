package artg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "model", "pk", "fields" })
public class Record implements Serializable {

	@JsonProperty("model")
	private String model;
	@JsonProperty("pk")
	private Integer pk;
	@JsonProperty("fields")
	private Fields fields;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -8712333777699459348L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Record() {
	}

	/**
	 * 
	 * @param model
	 * @param fields
	 * @param pk
	 */
	public Record(String model, Integer pk, Fields fields) {
		super();
		this.model = model;
		this.pk = pk;
		this.fields = fields;
	}

	@JsonProperty("model")
	public String getModel() {
		return model;
	}

	@JsonProperty("model")
	public void setModel(String model) {
		this.model = model;
	}

	@JsonProperty("pk")
	public Integer getPk() {
		return pk;
	}

	@JsonProperty("pk")
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	@JsonProperty("fields")
	public Fields getFields() {
		return fields;
	}

	@JsonProperty("fields")
	public void setFields(Fields fields) {
		this.fields = fields;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}