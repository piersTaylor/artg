package artg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "object_number", "object", "museum_number_token", "museum_number", "longitude", "title",
		"location", "sys_updated", "collection_code", "place", "artist", "slug", "primary_image_id", "latitude",
		"rights", "date_text" })
public class Fields implements Serializable {

	@JsonProperty("object_number")
	private String objectNumber;
	@JsonProperty("object")
	private String object;
	@JsonProperty("museum_number_token")
	private String museumNumberToken;
	@JsonProperty("museum_number")
	private String museumNumber;
	@JsonProperty("longitude")
	private String longitude;
	@JsonProperty("title")
	private String title;
	@JsonProperty("location")
	private String location;
	@JsonProperty("sys_updated")
	private String sysUpdated;
	@JsonProperty("collection_code")
	private String collectionCode;
	@JsonProperty("place")
	private String place;
	@JsonProperty("artist")
	private String artist;
	@JsonProperty("slug")
	private String slug;
	@JsonProperty("primary_image_id")
	private String primaryImageId;
	@JsonProperty("latitude")
	private String latitude;
	@JsonProperty("rights")
	private Integer rights;
	@JsonProperty("date_text")
	private String dateText;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 414830227158221657L;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Fields() {
	}

	/**
	 * 
	 * @param objectNumber
	 * @param location
	 * @param rights
	 * @param sysUpdated
	 * @param object
	 * @param collectionCode
	 * @param museumNumberToken
	 * @param title
	 * @param longitude
	 * @param slug
	 * @param latitude
	 * @param museumNumber
	 * @param artist
	 * @param place
	 * @param primaryImageId
	 * @param dateText
	 */
	public Fields(String objectNumber, String object, String museumNumberToken, String museumNumber, String longitude,
			String title, String location, String sysUpdated, String collectionCode, String place, String artist,
			String slug, String primaryImageId, String latitude, Integer rights, String dateText) {
		super();
		this.objectNumber = objectNumber;
		this.object = object;
		this.museumNumberToken = museumNumberToken;
		this.museumNumber = museumNumber;
		this.longitude = longitude;
		this.title = title;
		this.location = location;
		this.sysUpdated = sysUpdated;
		this.collectionCode = collectionCode;
		this.place = place;
		this.artist = artist;
		this.slug = slug;
		this.primaryImageId = primaryImageId;
		this.latitude = latitude;
		this.rights = rights;
		this.dateText = dateText;
	}

	@JsonProperty("object_number")
	public String getObjectNumber() {
		return objectNumber;
	}

	@JsonProperty("object_number")
	public void setObjectNumber(String objectNumber) {
		this.objectNumber = objectNumber;
	}

	@JsonProperty("object")
	public String getObject() {
		return object;
	}

	@JsonProperty("object")
	public void setObject(String object) {
		this.object = object;
	}

	@JsonProperty("museum_number_token")
	public String getMuseumNumberToken() {
		return museumNumberToken;
	}

	@JsonProperty("museum_number_token")
	public void setMuseumNumberToken(String museumNumberToken) {
		this.museumNumberToken = museumNumberToken;
	}

	@JsonProperty("museum_number")
	public String getMuseumNumber() {
		return museumNumber;
	}

	@JsonProperty("museum_number")
	public void setMuseumNumber(String museumNumber) {
		this.museumNumber = museumNumber;
	}

	@JsonProperty("longitude")
	public String getLongitude() {
		return longitude;
	}

	@JsonProperty("longitude")
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("title")
	public void setTitle(String title) {
		this.title = title;
	}

	@JsonProperty("location")
	public String getLocation() {
		return location;
	}

	@JsonProperty("location")
	public void setLocation(String location) {
		this.location = location;
	}

	@JsonProperty("sys_updated")
	public String getSysUpdated() {
		return sysUpdated;
	}

	@JsonProperty("sys_updated")
	public void setSysUpdated(String sysUpdated) {
		this.sysUpdated = sysUpdated;
	}

	@JsonProperty("collection_code")
	public String getCollectionCode() {
		return collectionCode;
	}

	@JsonProperty("collection_code")
	public void setCollectionCode(String collectionCode) {
		this.collectionCode = collectionCode;
	}

	@JsonProperty("place")
	public String getPlace() {
		return place;
	}

	@JsonProperty("place")
	public void setPlace(String place) {
		this.place = place;
	}

	@JsonProperty("artist")
	public String getArtist() {
		return artist;
	}

	@JsonProperty("artist")
	public void setArtist(String artist) {
		this.artist = artist;
	}

	@JsonProperty("slug")
	public String getSlug() {
		return slug;
	}

	@JsonProperty("slug")
	public void setSlug(String slug) {
		this.slug = slug;
	}

	@JsonProperty("primary_image_id")
	public String getPrimaryImageId() {
		return primaryImageId;
	}

	@JsonProperty("primary_image_id")
	public void setPrimaryImageId(String primaryImageId) {
		this.primaryImageId = primaryImageId;
	}

	@JsonProperty("latitude")
	public String getLatitude() {
		return latitude;
	}

	@JsonProperty("latitude")
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@JsonProperty("rights")
	public Integer getRights() {
		return rights;
	}

	@JsonProperty("rights")
	public void setRights(Integer rights) {
		this.rights = rights;
	}

	@JsonProperty("date_text")
	public String getDateText() {
		return dateText;
	}

	@JsonProperty("date_text")
	public void setDateText(String dateText) {
		this.dateText = dateText;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}