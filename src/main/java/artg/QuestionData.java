package artg;


public class QuestionData {
	
	private String title;
	private String place;
	private String artist;
	private String dateText;
	private String object;

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	/**
	 * @param title
	 * @param place
	 * @param artist
	 * @param dateText
	 * @param object
	 */
	public QuestionData(String title, String place, String artist, String dateText, String object) {
		super();
		this.title = title;
		this.place = place;
		this.artist = artist;
		this.dateText = dateText;
		this.object = object;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getDateText() {
		return dateText;
	}

	public void setDateText(String dateText) {
		this.dateText = dateText;
	}
}
